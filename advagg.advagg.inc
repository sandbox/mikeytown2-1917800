<?php

/**
 * @file
 * Advanced CSS/JS aggregation module.
 *
 * File used to store hook_advagg_* hooks.
 */

/**
 * Implements hook_advagg_get_info_on_files_alter().
 *
 * Used to make sure the info is up to date in the cache.
 */
function advagg_advagg_get_info_on_files_alter(&$return, $cached_data, $bypass_cache) {
  // Check for the ie_css_selector_limiter.
  $config = \Drupal::config('advagg.settings');
  if ($config->get('css.ie.limit_selectors')) {
    $limit_value = $config->get('css.ie.selector_limit');

    // Get the css path.
    list($css_path) = advagg_get_root_files_dir();
    $parts_path = $css_path[1] . '/parts/';

    foreach ($return as &$info) {
      // Skip if not a css file.
      if (empty($info['fileext']) || $info['fileext'] !== 'css') {
        continue;
      }

      // Check if this is a split css file.
      if (strpos($info['data'], $parts_path) !== FALSE) {
        $info['split'] = TRUE;
      }
      // Break large file into multiple small files if needed.
      elseif ($info['linecount'] > $limit_value) {
        advagg_split_css_file($info);
      }
    }
    unset($info);
  }
  // Capture hosts for DNS prefetching.
  foreach ($return as &$info) {
    // Skip if not a css file.
    if (empty($info['fileext']) || $info['fileext'] !== 'css') {
      continue;
    }
    // Get the file contents.
    $file_contents = (string) @file_get_contents($info['data']);

    // Get domain names in this css file.
    $matches = [];
    $pattern = '%url\(\s*+[\'"]?+(http:\/\/|https:\/\/|\/\/)([^\'"()\s]++)[\'"]?+\s*+\)%i';
    preg_match_all($pattern, $file_contents, $matches);
    $urls = array();
    if (!empty($matches[1])) {
      foreach ($matches[1] as $key => $match) {
        $parse = @parse_url($match . $matches[2][$key]);
        if (!empty($parse['host']) && empty($urls[$parse['host']])) {
          $urls[$parse['host']] = $parse['host'];
        }
      }
      $urls = array_values($urls);
    }
    if (!empty($urls)) {
      $info['dns_prefetch'] = $urls;
    }
  }
  unset($info);
}
