<?php

/**
 * @file
 * Advanced CSS/JS aggregation module.
 *
 * These functions are needed for cache misses.
 */

// Modify CSS/JS arrays.
/**
 * Group the CSS/JS into the biggest buckets possible.
 *
 * @param array $files_to_aggregate
 *   An array of CSS/JS groups.
 *
 * @return array
 *   New version of groups.
 */
function advagg_generate_groups(array $files_to_aggregate) {
        $last_ext = $ext;
        if ($ext === 'css') {
          if (isset($file_info['media'])) {
            if ($config->get('css.combine_media')) {
              $file_info['media_query'] = $file_info['media'];
            }
            elseif ($media != $file_info['media']) {
              // Media changed.
              $changed = TRUE;
              $media = $file_info['media'];
            }
          }
          if (empty($file_info['media']) && !empty($media)) {
            // Media changed to empty.
            $changed = TRUE;
            $media = '';
          }
        }


        if ($config->get('css.ie.limit_selectors')) {
          $file_info += $files_info[$file_info['data']];
          // Prevent CSS rules exceeding 4095 due to limits with IE9 and below.
          if ($ext === 'css') {
            $selector_count += $file_info['linecount'];
            if ($selector_count > $limit_value) {
              $changed = TRUE;
              $selector_count = $file_info['linecount'];

              // Break large file into multiple smaller files.
              if ($file_info['linecount'] > $limit_value) {
                $parts = advagg_split_css_file($file_info);
              }
            }
          }
        }

}

/**
 * Given a file info array it will split the file up.
 *
 * @param array $file_info
 *   File info array from advagg_get_info_on_file().
 *
 * @return array
 *   Array with advagg_get_info_on_file data and split data.
 */
function advagg_split_css_file(array $file_info) {
  // Make advagg_parse_media_blocks() available.
  module_load_include('inc', 'advagg', 'advagg.missing');

  // Get the CSS file and break up by media queries.
  $file_contents = file_get_contents($file_info['data']);
  $media_blocks = advagg_parse_media_blocks($file_contents);

  $config = \Drupal::config('advagg.settings');
  // Get 98% of the advagg_ie_css_selector_limiter_value; usually 4013.
  $selector_split_value = (int) max(floor($config->get('css.ie.selector_limit') * 0.98), 100);
  $part_selector_count = 0;
  $major_chunks = array();
  $counter = 0;
  // Group media queries together.
  foreach ($media_blocks as $media_block) {
    $matched = array();
    // Get the number of selectors.
    // http://stackoverflow.com/a/12567381/125684
    $selector_count = preg_match_all('/\{.+?\}|,/s', $media_block, $matched);
    $part_selector_count += $selector_count;

    if ($part_selector_count > $selector_split_value) {
      if (isset($major_chunks[$counter])) {
        ++$counter;
        $major_chunks[$counter] = $media_block;
      }
      else {
        $major_chunks[$counter] = $media_block;
      }
      ++$counter;
      $part_selector_count = 0;
    }
    else {
      if (isset($major_chunks[$counter])) {
        $major_chunks[$counter] .= "\n" . $media_block;
      }
      else {
        $major_chunks[$counter] = $media_block;
      }
    }
  }

  $parts = array();
  $overall_split = 0;
  $split_at = $selector_split_value;
  $chunk_split_value = (int) $config->get('css.ie.selector_limit') - $selector_split_value - 1;
  foreach ($major_chunks as $chunk_key => $chunks) {
    $last_chunk = FALSE;
    $file_info['split_last_part'] = FALSE;
    if (count($major_chunks) - 1 == $chunk_key) {
      $last_chunk = TRUE;
    }

    // Get the number of selectors.
    $matches = array();
    $selector_count = preg_match_all('/\{.+?\}|,/s', $chunks, $matches);

    // Pass through if selector count is low.
    if ($selector_count < $selector_split_value) {
      $overall_split += $selector_count;
      if ($last_chunk) {
        $file_info['split_last_part'] = TRUE;
      }
      $subfile = advagg_create_subfile($chunks, $overall_split, $file_info);
      if (empty($subfile)) {
        // Somthing broke; do not create a subfile.
        \Drupal::logger('advagg')->notice('Spliting up a CSS file failed. File info: <code>@info</code>', array('@info' => var_export($file_info, TRUE)));
        return array();
      }
      $parts[] = $subfile;
      continue;
    }

    $media_query = '';
    if (strpos($chunks, '@media') !== FALSE) {
      $media_query_pos = strpos($chunks, '{');
      $media_query = substr($chunks, 0, $media_query_pos);
      $chunks = substr($chunks, $media_query_pos + 1);
    }

    // Split CSS into selector chunks.
    $split = preg_split('/(\{.+?\}|,)/si', $chunks, -1, PREG_SPLIT_DELIM_CAPTURE);

    // Setup and handle media queries.
    $new_css_chunk = array(0 => '');
    $selector_chunk_counter = 0;
    $counter = 0;
    if (!empty($media_query)) {
      $new_css_chunk[0] = $media_query . '{';
      $new_css_chunk[1] = '';
      ++$selector_chunk_counter;
      ++$counter;
    }
    // Have the key value be the running selector count and put split array semi
    // back together.
    foreach ($split as $value) {
      $new_css_chunk[$counter] .= $value;
      if (strpos($value, '}') === FALSE) {
        ++$selector_chunk_counter;
      }
      else {
        if ($counter + 1 < $selector_chunk_counter) {
          $selector_chunk_counter += ($counter - $selector_chunk_counter + 1) / 2;
        }
        $counter = $selector_chunk_counter;
        if (!isset($new_css_chunk[$counter])) {
          $new_css_chunk[$counter] = '';
        }
      }
    }

    // Group selectors.
    $first = TRUE;
    while (!empty($new_css_chunk)) {
      // Find where to split the array.
      $string_to_write = '';
      while (array_key_exists($split_at, $new_css_chunk) === FALSE) {
        --$split_at;
      }

      // Combine parts of the css so that it can be saved to disk.
      foreach ($new_css_chunk as $key => $value) {
        if ($key !== $split_at) {
          // Move this css row to the $string_to_write variable.
          $string_to_write .= $value;
          unset($new_css_chunk[$key]);
        }
        // We are at the split point.
        else {
          // Get the number of selectors in this chunk.
          $matched = array();
          $chunk_selector_count = preg_match_all('/\{.+?\}|,/s', $new_css_chunk[$key], $matched);
          if ($chunk_selector_count < $chunk_split_value) {
            // The number of selectors at this point is below the threshold;
            // move this chunk to the write variable and break out of the loop.
            $string_to_write .= $value;
            unset($new_css_chunk[$key]);
            $overall_split = $split_at;
            $split_at += $selector_split_value;
          }
          else {
            // The number of selectors with this chunk included is over the
            // threshold; do not move it. Change split position so the next
            // iteration of the while loop ends at the correct spot. Because we
            // skip unset here, this chunk will start the next part file.
            $overall_split = $split_at;
            $split_at += $selector_split_value - $chunk_selector_count;
          }
          break;
        }
      }

      // Handle media queries.
      if (!empty($media_query)) {
        // See if brackets need a new line.
        if (strpos($string_to_write, "\n") === 0) {
          $open_bracket = '{';
        }
        else {
          $open_bracket = "{\n";
        }
        if (strrpos($string_to_write, "\n") === strlen($string_to_write)) {
          $close_bracket = '}';
        }
        else {
          $close_bracket = "\n}";
        }

        // Fix syntax around media queries.
        if ($first) {
          $string_to_write .= $close_bracket;
        }
        elseif (empty($new_css_chunk)) {
          $string_to_write = $media_query . $open_bracket . $string_to_write;
        }
        else {
          $string_to_write = $media_query . $open_bracket . $string_to_write . $close_bracket;
        }
      }
      // Handle the last split part.
      if (empty($new_css_chunk) && $last_chunk) {
        $file_info['split_last_part'] = TRUE;
      }
      // Write the data.
      $subfile = advagg_create_subfile($string_to_write, $overall_split, $file_info);
      if (empty($subfile)) {
        // Somthing broke; do not create a subfile.
        \Drupal::logger('advagg')->notice('Spliting up a CSS file failed. File info: <code>@info</code>', array('@info' => var_export($file_info, TRUE)));
        return array();
      }
      $parts[] = $subfile;
      $first = FALSE;
    }
  }
  return $parts;
}

/**
 * Write CSS parts to disk; used when CSS selectors in one file is > 4096.
 *
 * @param string $css
 *   CSS data to write to disk.
 * @param int $overall_split
 *   Running count of what selector we are from the original file.
 * @param array $file_info
 *   File info array from advagg_get_info_on_file().
 *
 * @return array
 *   Array with advagg_get_info_on_file data and split data; FALSE on failure.
 */
function advagg_create_subfile($css, $overall_split, array $file_info) {
  static $parts_uri;
  static $parts_path;
  if (!isset($parts_uri)) {
    list($css_path) = advagg_get_root_files_dir();
    $parts_uri = $css_path[0] . '/parts';
    $parts_path = $css_path[1] . '/parts';

    // Create the public://advagg_css/parts dir.
    file_prepare_directory($parts_uri, FILE_CREATE_DIRECTORY);

    // Make advagg_save_data() available.
    module_load_include('inc', 'advagg', 'advagg.missing');
  }

  // Get the path from $file_info['data'].
  $uri_path = advagg_get_relative_path($file_info['data']);
  if (!file_exists($uri_path) || is_dir($uri_path)) {
    return FALSE;
  }

  // Write the current chunk of the CSS into a file.
  $new_filename = str_ireplace('.css', '.' . $overall_split . '.css', $uri_path);

  // Fix for things that write dynamically to the public file system.
  $scheme = \Drupal::service("file_system")->uriScheme($new_filename);
  if ($scheme) {
    $wrapper = \Drupal::service("stream_wrapper_manager")->getViaScheme($scheme);
    if ($wrapper) {
      // Use the wrappers directory path.
      $new_filename = $wrapper->getDirectoryPath() . '/' . file_uri_target($new_filename);
    }
    else {
      // If the scheme does not have a wrapper; prefix file with the scheme.
      $new_filename = $scheme . '/' . file_uri_target($new_filename);
    }
  }

  $part_uri = $parts_uri . '/' . $new_filename;
  $dirname = \Drupal::service("file_system")->dirname($part_uri);
  file_prepare_directory($dirname, FILE_CREATE_DIRECTORY);

  // Get info on the file that was just created.
  $part = advagg_get_info_on_file($parts_path . '/' . $new_filename) + $file_info;
  $part['split'] = TRUE;
  $part['split_location'] = $overall_split;
  $part['split_original'] = $file_info['data'];

  // Overwrite/create file if hash doesn't match.
  $hash = \Drupal\Component\Utility\Crypt::hashBase64($css);
  if ($part['content_hash'] != $hash) {
    advagg_save_data($part_uri, $css, TRUE);
    $part = advagg_get_info_on_file($parts_path . '/' . $new_filename, TRUE) + $file_info;
    $part['split'] = TRUE;
    $part['split_location'] = $overall_split;
    $part['split_original'] = $file_info['data'];
  }

  return $part;
}
