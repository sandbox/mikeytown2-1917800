<?php

/**
 * @file
 * Contains \Drupal\advagg_validator\Form\CssW3Form.
 */

namespace Drupal\advagg_validator\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\SafeMarkup;

/**
 * Configure form for W3C validation of CSS files.
 */
class CssW3Form extends BaseValidatorForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'advagg_validator_cssw3';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::generateForm('css', FALSE);
    $form['notice'] = array(
      '#markup' => '<div>' . t('Notice: The form below will submit files to the <a href="http://jigsaw.w3.org/css-validator/">http://jigsaw.w3.org/css-validator/</a> service if used.') . '</div>',
      '#weight' => -1,
    );
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitCheckAll(array &$form, FormStateInterface $form_state) {
    $dir = $form_state->getTriggeringElement()['#name'];
    $files = array();
    foreach ($form_state->getValues() as $key => $value) {
      if (strpos($key, 'hidden') === FALSE || strpos($value, $dir) === FALSE || ($dir === '.' && substr_count($value, '/') > 0)) {
        continue;
      }
      $files[] = $value;
    }

    // Check list.
    $info = $this->testFiles($files);
    $info = $this->hideGoodFiles($info);

    // FIXME: is escaped through drupal_set_message(). Which leads to <pre>
    // showing up... and the formatting (/n based) being scrubbed.
    $output = '<pre>' . print_r($info, TRUE) . '</pre>';
    // TODO: ajax this!
    drupal_set_message($output);
  }

  /**
   * {@inheritdoc}
   */
  public function submitCheckDirectory(array &$form, FormStateInterface $form_state) {
    $dir = $form_state->getTriggeringElement()['#name'];
    $files = array();
    $slash_count = substr_count('/' . $dir, '/');
    foreach ($form_state->getValues() as $key => $value) {
      if (strpos($key, 'hidden') === FALSE || strpos($value, $dir) === FALSE || substr_count($value, '/') > $slash_count || ($dir === '.' && substr_count($value, '/') > 0)) {
        continue;
      }
      $files[] = $value;
    }

    // Check list.
    $info = $this->testFiles($files);
    $info = $this->hideGoodFiles($info);

    // FIXME: is escaped through drupal_set_message(). Which leads to <pre>
    // showing up... and the formatting (/n based) being scrubbed.
    $output = '<pre>' . print_r($info, TRUE) . '</pre>';
    // TODO: ajax this!
    drupal_set_message($output);
  }

  /**
   * {@inheritdoc}
   */
  private function testFiles(array $files, $options = array()) {
    $output = [];
    foreach ($files as $filename) {
      // Skip missing files.
      if (!file_exists($filename)) {
        continue;
      }

      $file_contents = file_get_contents($filename);
      $lines = file($filename);
      $filename_hash = \Drupal\Component\Utility\Crypt::hashBase64($filename);
      $content_hash = \Drupal\Component\Utility\Crypt::hashBase64($file_contents);

      // See if this file needs to be scanned.
      $file_ok = db_select('advagg_validator', 'av')
        ->fields('av', array('content_hash', 'data'))
        ->condition('filename_hash', $filename_hash)
        ->condition('filetype', 'css')
        ->execute()
        ->fetchAllKeyed();
      if (!empty($file_ok)) {
        foreach ($file_ok as $content_hash_db => $serialized_data) {
          if ($content_hash_db == $content_hash) {
            $output[$filename] = unserialize($serialized_data);
            continue 2;
          }
        }
      }

      // Run jigsaw.w3.org validator.
      $output[$filename]['jigsaw.w3.org'] = $this->testW3C($filename, $options);

      // Get extra context for errors.
      if (!empty($output[$filename]['jigsaw.w3.org']['errors'])) {
        foreach ($output[$filename]['jigsaw.w3.org']['errors'] as &$value) {
          if (isset($value['line'])) {
            $value['linedata'] = $lines[($value['line'] - 1)];
            if (strlen($value['linedata']) > 512) {
              unset($value['linedata']);
            }
          }
        }
        unset($value);
      }
      if (!empty($output[$filename]['jigsaw.w3.org']['warnings'])) {
        foreach ($output[$filename]['jigsaw.w3.org']['warnings'] as &$value) {
          if (isset($value['line'])) {
            $value['linedata'] = $lines[$value['line'] - 1];
            if (strlen($value['linedata']) > 512) {
              unset($value['linedata']);
            }
          }
        }
        unset($value);
      }

      // Save data.
      if (empty($output[$filename]['jigsaw.w3.org']['error'])) {
        $record = array(
          'filename' => $filename,
          'filename_hash' => $filename_hash,
          'content_hash' => $content_hash,
          'filetype' => 'css',
          'data' => serialize($output[$filename]),
        );
        db_merge('advagg_validator')
          ->key(array(
            'filename_hash' => $record['filename_hash'],
            ))
          ->fields($record)
          ->execute();
      }
    }
    return $output;
  }

  /**
   * Given a CSS file, test to make sure it is valid CSS.
   *
   * @param string $filename
   *   The name of the file.
   * @param array $validator_options
   *   List of options to pass along to the CSS Validator.
   *
   * @return array
   *   Info from the w3c server.
   */
  private function testW3C($filename, &$validator_options = array()) {
    // Get CSS files contents.
    $validator_options['text'] = file_get_contents($filename);
    if (strlen($validator_options['text'] > 50000)) {
      unset($validator_options['text']);
      $validator_options['uri'] = \Drupal::request()->getBaseUrl() . $filename;
    }

    // Add in defaults.
    $validator_options += array(
      'output' => 'soap12',
      'warning' => '1',
      'profile' => 'css3',
      'usermedium' => 'all',
      'lang' => 'en',
    );

    // Build request URL.
    // API Documentation http://jigsaw.w3.org/css-validator/api.html
    $request_url = 'http://jigsaw.w3.org/css-validator/validator';
    $query = http_build_query($validator_options, '', '&');
    $url = $request_url . '?' . $query;

    try {
      $data = \Drupal::httpClient()
        ->get($url)
        ->getBody();
    }
    catch (Exception $e) {
      watchdog_exception('AdvAgg Validator', $e);
    }
    if (!empty($data)) {
      // Parse XML and return info.
      $return = $this->parseSoapResponse($data);
      $return['filename'] = $filename;
      if (isset($validator_options['text'])) {
        unset($validator_options['text']);
      }
      elseif (isset($validator_options['uri'])) {
        unset($validator_options['uri']);
      }
      $return['options'] = $validator_options;
      return $return;
    }

    return array('error' => t('W3C Server did not return a 200 or request data was empty.'));
  }

  /**
   * {@inheritdoc}
   */
  private function parseSoapResponse($xml) {
    $doc = new DOMDocument();
    $response = array();

    // Try to load soap 1.2 XML response, and suppress warning reports if any.
    if (!@$doc->loadXML($xml)) {
      // Could not load the XML document.
      return $response;
    }

    // Get the standard CDATA elements.
    $cdata = array('uri', 'checkedby', 'csslevel', 'date');
    foreach ($cdata as $var) {
      $element = $doc->getElementsByTagName($var);
      if ($element->length) {
        $response[$var] = $element->item(0)->nodeValue;
      }
    }

    // Handle the element validity and get errors if not valid.
    $element = $doc->getElementsByTagName('validity');
    if ($element->length && $element->item(0)->nodeValue === 'true') {
      $response['validity'] = TRUE;
    }
    else {
      $response['validity'] = FALSE;
      $errors = $doc->getElementsByTagName('error');
      foreach ($errors as $error) {
        $response['errors'][] = $this->domExtractor($error);
      }
    }

    // Get warnings.
    $warnings = $doc->getElementsByTagName('warning');
    foreach ($warnings as $warning) {
      $response['warnings'][] = $this->domExtractor($warning);
    }

    // Return response array.
    return $response;
  }

}
