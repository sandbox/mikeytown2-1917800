<?php

/**
 * @file
 * Contains \Drupal\advagg_js_minify\Asset\JsOptimizer.
 */

namespace Drupal\advagg_js_minify\Asset;

use Drupal\Component\Utility\Unicode;
use Drupal\Core\Asset\AssetOptimizerInterface;

/**
 * Optimizes a JavaScript asset.
 */
class JsOptimizer implements AssetOptimizerInterface {

  /**
   * {@inheritdoc}
   */
  public function optimize(array $js_asset) {
    if ($js_asset['type'] !== 'file') {
      throw new \Exception('Only file JavaScript assets can be optimized.');
    }
    if ($js_asset['type'] === 'file' && !$js_asset['preprocess']) {
      throw new \Exception('Only file JavaScript assets with preprocessing enabled can be optimized.');
    }

    // If a BOM is found, convert the file to UTF-8, then use substr() to
    // remove the BOM from the result.
    $data = file_get_contents($js_asset['data']);
    if ($encoding = (Unicode::encodingFromBOM($data))) {
      $data = Unicode::substr(Unicode::convertToUtf8($data, $encoding), 1);
    }

    // If no BOM is found, check for the charset attribute.
    elseif (isset($js_asset['attributes']['charset'])) {
      $data = Unicode::convertToUtf8($data, $js_asset['attributes']['charset']);
    }
    $config = \Drupal::config('advagg_js_minify.settings');
    $minifier = $config->get('minifier');
    if ($file_settings = $config->get('file_settings')) {
      $file_settings = array_column($file_settings, 'minifier', 'path');
      if (isset($file_settings[$js_asset['data']])) {
        $minifier = $file_settings[$js_asset['data']];
      }
    }

    // Do nothing if js file minification is disabled.
    if (empty($minifier) || \Drupal::config('advagg.settings')->get('cache_level') < 0) {
      return $data;
    }

    // Do not re-minify if the file is already minified.
    $semicolon_count = substr_count($data, ';');
    // @ignore sniffer_whitespace_openbracketspacing_openingwhitespace
    if ( $minifier != 2
      && $semicolon_count > 10
      && $semicolon_count > (substr_count($data, "\n", strpos($data, ';')) * 5)
      ) {
      if ($config->get('add_license')) {
        $url = file_create_url($js_asset['data']);
        $data = "/* Source and licensing information for the line(s) below can be found at $url. */\n" . $data . "\n/* Source and licensing information for the above line(s) can be found at $url. */";
      }
      return $data;
    }

    $data_original = $data;
    $before = strlen($data);

    // Do not use jsmin() if the function can not be called.
    if ($minifier == 3 && !function_exists('jsmin')) {
      $minifier = 5;
      \Drupal::logger('advagg_js_minify')->notice('The jsmin function does not exist. Using JSqueeze.', []);
    }

    // Do not use jsmin() if the js file contains cyrillic characters.
    if ($minifier == 3 && preg_match('/[А-Яа-яЁё]/u', $data)) {
      $minifier = 5;
      \Drupal::logger('advagg_js_minify')->notice('The jsmin function does not handle cyrillic characters. Using JSqueeze.', []);
    }

    $cache = \Drupal::service('cache.advagg.minify');
    $info = advagg_get_info_on_files([$js_asset['data']]);
    $info = $info[$js_asset['data']];
    $cid = 'js_minify:' . $minifier . ':' . $info['filename_hash'];
    $cid .= !empty($info['content_hash']) ? ':' . $info['content_hash'] : '';
    $cached_data = $cache->get($cid);
    if (!empty($cached_data->data)) {
      $data = $cached_data->data;
    }
    else {
      // Use the minifier.
      list(, , , $functions) = advagg_js_minify_configuration();
      if (isset($functions[$minifier])) {
        $run = $functions[$minifier];
        if (function_exists($run)) {
          $run($data);
        }
      }
      else {
        return $data;
      }

      // Ensure that $data ends with ; or }.
      if (strpbrk(substr(trim($data), -1), ';}') === FALSE) {
        $data = trim($data) . ';';
      }

      // Cache minified data for at least 1 week.
      $cache->set($cid, $data, REQUEST_TIME + (86400 * 7), ['advagg_js', $info['filename_hash']]);

      // Make sure minification ratios are good.
      $after = strlen($data);
      $ratio = 0;
      if ($before != 0) {
        $ratio = ($before - $after) / $before;
      }

      // Make sure the returned string is not empty or has a VERY high
      // minification ratio.
      // @ignore sniffer_whitespace_openbracketspacing_openingwhitespace
      if ( empty($data)
        || empty($ratio)
        || $ratio < 0
        || $ratio > $config->get('ratio_max')
      ) {
        $data = $data_original;
      }
      elseif ($config->get('add_license')) {
        $url = file_create_url($js_asset['data']);
        $data = "/* Source and licensing information for the line(s) below can be found at $url. */\n" . $data . "\n/* Source and licensing information for the above line(s) can be found at $url. */";
      }
    }
    return $data;
  }

  /**
   * Processes the contents of a javascript asset for cleanup.
   *
   * @param string $contents
   *   The contents of the javascript asset.
   *
   * @return string
   *   Contents of the javascript asset.
   */
  public function clean($contents) {
    // Remove JS source and source mapping urls or these may cause 404 errors.
    $contents = preg_replace('/\/\/(#|@)\s(sourceURL|sourceMappingURL)=\s*(\S*?)\s*$/m', '', $contents);

    return $contents;
  }

}
