<?php

/**
 * @file
 * Contains \Drupal\advagg\State\State.
 */

namespace Drupal\advagg\State;

use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\Core\State\State as CoreState;
use Drupal\Core\State\StateInterface as CoreStateInterface;

/**
 * Provides AdvAgg with a file status state system using a key value store.
 */
class State extends CoreState implements CoreStateInterface {

  /**
   * Constructs a State object.
   *
   * @param \Drupal\Core\KeyValueStore\KeyValueFactoryInterface $key_value_factory
   *   The key value store to use.
   */
  public function __construct(KeyValueFactoryInterface $key_value_factory) {
    $this->keyValueStore = $key_value_factory->get('state_advagg_files');
  }

  /**
   * Gets all stored AdvAgg file information.
   *
   * @return array
   *   An array of all key value pairs.
   */
  public function getAll() {
    $values = $this->keyValueStore->getAll();
    $this->cache += $values;
    return $values;
  }

}
