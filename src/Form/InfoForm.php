<?php

/**
 * @file
 * Contains \Drupal\advagg\Form\InfoForm.
 */

namespace Drupal\advagg\Form;

use Drupal\Component\Utility\SafeMarkup;
use Drupal\Component\Utility\Unicode;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * View AdvAgg information for this site.
 */
class InfoForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'advagg_info';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = [];
    $form['tip'] = [
      '#markup' => '<p>' . t('This page provides debugging information. There are no configuration options here.') . '</p>',
    ];

    // Get all hooks and variables.
    $core_hooks = \Drupal::service('theme.registry')->get();
    $advagg_hooks = advagg_hooks_implemented();

    // Output html preprocess functions hooks.
    $form['theme_info'] = [
      '#type' => 'details',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#title' => t('Hook Theme Info'),
    ];
    $data = implode("\n", $core_hooks['html']['preprocess functions']);
    $form['theme_info']['advagg_theme_info'] = [
      '#markup' => '<p>preprocess functions on html.</p><pre>' . $data . '</pre>',
    ];

    $file_data = \Drupal::service('state.advagg.files')->getAll();

    // Get all parent css and js files.
    $types = ['css', 'js'];
    foreach ($types as $type) {
      $form[$type] = [
        '#type' => 'details',
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#title' => t('@type files', ['@type' => Unicode::strtoupper($type)]),
      ];
    }
    foreach($file_data as $name => $info) {
      if (!in_array($info['fileext'],$types)) {
        continue;
      }
      $form[$info['fileext']][$info['filename_hash']] = [
        '#markup' => '<details><summary>' . \Drupal::translation()->formatPlural($info['changes'], 'changed 1 time - %file<br />', 'changed %changes times - %file<br />', [
          '%changes' => $info['changes'],
          '%file' => $name,
        ]) . '</summary><div class="details-wrapper"><pre>'. print_r($info, TRUE) . '</pre></div></details>',
      ];
    }

    // Display as module -> hook instead of hook -> module.
    ksort($advagg_hooks);
    $module_hooks = [];
    foreach ($advagg_hooks as $hook => $values) {
      if (!empty($values)) {
        foreach ($values as $module_name) {
          if (!isset($module_hooks[$module_name])) {
            $module_hooks[$module_name] = [];
          }
          $module_hooks[$module_name][] = $hook;
        }
      }
      else {
        $module_hooks['not in use'][] = $hook;
      }
    }
    ksort($module_hooks);

    $form['modules_implementing_advagg'] = [
      '#type' => 'details',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#title' => t('Modules implementing aggregate hooks'),
    ];
    $form['hooks_implemented'] = [
      '#type' => 'details',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#title' => t('AdvAgg CSS/JS hooks implemented by modules'),
    ];

    // Output all advagg hooks implemented.
    foreach ($module_hooks as $hook => $values) {
      if (empty($values)) {
        $form['modules_implementing_advagg'][$hook] = [
          '#markup' => '<div><strong>' . SafeMarkup::checkPlain($hook) . ':</strong> 0</div>',
        ];
      }
      else {
        $form['modules_implementing_advagg'][$hook] = [
          '#markup' => '<div><strong>' . SafeMarkup::checkPlain($hook) . ':</strong> ' . count($values) . $this->formatList($values) . '</div>',
        ];
      }
    }

    // Output all advagg hooks implemented.
    foreach ($advagg_hooks as $hook => $values) {
      if (empty($values)) {
        $form['hooks_implemented'][$hook] = [
          '#markup' => '<div><strong>' . SafeMarkup::checkPlain($hook) . ':</strong> 0</div>',
        ];
      }
      else {
        $form['hooks_implemented'][$hook] = [
          '#markup' => '<div><strong>' . SafeMarkup::checkPlain($hook) . ':</strong> ' . count($values) . $this->formatList($values) . '</div>',
        ];
      }
    }

    // Output what is used inside of the advagg_get_current_hooks_hash() function.
    $form['hooks_variables_hash'] = [
      '#type' => 'details',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#title' => t('Hooks And Variables Used In Hash'),
    ];
    $form['hooks_variables_hash']['description'] = [
      '#markup' => t('Current Value: %value. Below is the listing of variables and hooks used to generate the 3rd hash of an aggregates filename.', ['%value' => advagg_get_current_hooks_hash()]),
    ];
    $form['hooks_variables_hash']['output'] = [
      // @ignore production_php
      '#markup' => '<pre>' . print_r(advagg_current_hooks_hash_array(), TRUE) . '</pre>',
    ];
    /*
    // Get info about a file.
    $form['get_info_about_agg'] = array(
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#title' => t('Get detailed info about an aggregate file'),
    );
    $form['get_info_about_agg']['filename'] = array(
      '#type' => 'textfield',
      '#size' => 170,
      '#maxlength' => 256,
      '#default_value' => '',
      '#title' => t('Filename'),
    );
    $form['get_info_about_agg']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Lookup Details'),
      '#submit' => array('advagg_admin_get_file_info_submit'),
      '#validate' => array('advagg_admin_get_file_info_validate'),
      '#ajax' => array(
        'callback' => 'advagg_admin_get_file_info_callback',
        'wrapper' => 'advagg-file-info-ajax',
        'effect' => 'fade',
      ),
    );
    module_load_include('install', 'advagg', 'advagg');
    $form['get_info_about_agg']['tip'] = array(
      '#markup' => '<p>' . t('Takes input like "@css_file" or a full aggregate name like "@advagg_js"', array(
        '@css_file' => $css_file,
        '@advagg_js' => advagg_install_get_first_advagg_file($js_path[1]),
      )) . '</p>',
    );
    $form['get_info_about_agg']['wrapper'] = array(
      '#markup' => "<div id='advagg-file-info-ajax'></div>",
    );*/
    $form =  parent::buildForm($form, $form_state);
    unset($form['actions']);
    return $form;
  }

  /**
  * Format an indented list from array.
  *
  * @param array $list
  *   The array to convert to a string.
  * @param integer $depth
  *   (optional) Depth multiplier for indentation.
  *
  * @return string
  *   The imploded and spaced array.
  */
  private function formatList(array $list, $depth = 1) {
    $spacer = '<br />' . str_repeat('&nbsp;', 2 * $depth);
    $output = $spacer . Xss::filter(implode($spacer, $list), ['br']);
    return $output;
  }

  /**
   * Display file info in a drupal message.
   */
  function advagg_admin_get_file_info_submit($form, &$form_state) {
    // @codingStandardsIgnoreLine
    if (!empty($form_state['input']['ajax_page_state'])) {
      return;
    }
    $info = advagg_admin_get_file_info($form_state['values']['filename']);
    $output = '<pre>' . \Drupal\Component\Utility\SafeMarkup::checkPlain(print_r($info, TRUE)) . '</pre>';
    // @ignore security_dsm
    drupal_set_message($output);
  }

  /**
   * Display file info via ajax callback.
   */
  function advagg_admin_get_file_info_callback($form, &$form_state) {
    if (!empty($form_state['values']['error'])) {
      return '<div id="advagg-file-info-ajax"></div>';
    }
    $info = advagg_admin_get_file_info($form_state['values']['filename']);
    if (empty($info)) {
      form_set_error('filename', t('Please input a valid aggregate filename.'));
      return '<div id="advagg-file-info-ajax"></div>';
    }
    else {
      $output = '<pre>' . print_r($info, TRUE) . '</pre>';
      return '<div id="advagg-file-info-ajax">' . $output . '</div>';
    }
  }

  /**
   * Verify that the filename is correct.
   */
  function advagg_admin_get_file_info_validate($form, &$form_state) {
    if (empty($form_state['values']['filename'])) {
      form_set_error('filename', t('Please input an aggregate filename.'));
      $form_state['values']['error'] = TRUE;
    }
  }

  /**
   * Get detailed info about the given filename.
   *
   * @param string $filename
   *   Name of file to lookup.
   *
   * @return array
   *   Returns an array of detailed info about this file.
   */
  function advagg_admin_get_file_info($filename) {
    module_load_include('inc', 'advagg', 'advagg.missing');
    module_load_include('inc', 'advagg', 'advagg');

    // Strip quotes and trim.
    $filename = trim(str_replace(array('"', "'"), '', $filename));

    $data = advagg_get_hashes_from_filename(basename($filename));
    $output = array();
    if (is_array($data)) {
      list($type, $aggregate_filenames_hash, $aggregate_contents_hash) = $data;

      // Get a list of files.
      $files = advagg_get_files_from_hashes($type, $aggregate_filenames_hash, $aggregate_contents_hash);
      if (empty($files)) {
        list($css_path, $js_path) = advagg_get_root_files_dir();
        // Skip if the file exists.
        if ($type === 'css') {
          $uri = $css_path[0] . '/' . $filename;
        }
        elseif ($type === 'js') {
          $uri = $js_path[0] . '/' . $filename;
        }
        if (file_exists($uri)) {
          $atime = advagg_get_atime($aggregate_filenames_hash, $aggregate_contents_hash, $uri);
          if (REQUEST_TIME - $atime > $this->config('system.performance')->get('stale_file_threshold')) {
             $files = t('This is an old aggregate, it should be deleted on the next cron run.');
           }
           else {
             $files = t('This is an old aggregate, it should be deleted on the cron run after !time.', array('!time' => \Drupal::service("date.formatter")->formatInterval($this->config('system.performance')->get('stale_file_threshold') - (REQUEST_TIME - $atime))));
           }

        }
        else {
          $files = t('This aggregate file no longer exists.');
        }
      }
      $data['files'] = $files;

      // Get detailed info on each file.
      $files_info_filenames = array();
      foreach ($data['files'] as $filename => &$info) {
        $files_info_filenames[] = $filename;
      }
      unset($info);

      // Get filesystem data.
      $files_info = advagg_get_info_on_files($files_info_filenames);

      foreach ($data['files'] as $filename => &$info) {
        $info += $files_info[$filename];
        if (\Drupal::moduleHandler()->moduleExists('advagg_bundler')) {
          $bundler = advagg_bundler_analysis($filename);
          $info['group_hash'] = $bundler['group_hash'];
        }
      }
      unset($info);
      $output = $data;
    }
    else {
      $results = db_select('advagg_files', 'af')
        ->fields('af')
        ->condition('filename', '%' . db_like($filename), 'LIKE')
        ->execute();
      while ($row = $results->fetchAssoc()) {
        $row += advagg_get_info_on_file($row['filename']);
        if (\Drupal::moduleHandler()->moduleExists('advagg_bundler')) {
          $bundler = advagg_bundler_analysis($row['filename']);
          $row['group_hash'] = $bundler['group_hash'];
        }
        $output[] = $row;
      }
    }
    return $output;
  }

}
