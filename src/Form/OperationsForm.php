<?php

/**
 * @file
 * Contains \Drupal\advagg\Form\OperationsForm.
 */

namespace Drupal\advagg\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Component\Utility\Crypt;

/**
 * Configure advagg settings for this site.
 */
class OperationsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'advagg_operations';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['advagg.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = [];
    // Explain what can be done on this page.
    $form['tip'] = array(
      '#markup' => '<p>' . t('This is a collection of commands to control the cache and to manage testing of this module. In general this page is useful when troubleshooting some aggregation issues. For normal operations, you do not need to do anything on this page below the Smart Cache Flush. There are no configuration options here.') . '</p>',
    );

    // Buttons to do stuff.
    // AdvAgg smart cache flushing.
    $form['smart_flush'] = array(
      '#type' => 'fieldset',
      '#title' => t('Smart Cache Flush'),
      '#description' => t('Scan all files referenced in aggregated files. If any of them have changed, clear that cache so the changes will go out.'),
    );
    $form['smart_flush']['advagg_flush'] = [
      '#type' => 'submit',
      '#value' => t('Flush AdvAgg Cache'),
      '#submit' => ['::advaggFlushCache'],
    ];

    // Set/Remove Bypass Cookie.
    $form['bypass'] = array(
      '#type' => 'fieldset',
      '#title' => t('Aggregation Bypass Cookie'),
      '#description' => t('This will set or remove a cookie that disables aggregation for a set period of time.'),
    );
    $bypass_length = array_combine(array(
      60 * 60 * 6,
      60 * 60 * 12,
      60 * 60 * 24,
      60 * 60 * 24 * 2,
      60 * 60 * 24 * 7,
      60 * 60 * 24 * 30,
      60 * 60 * 24 * 365,
    ), array(
      60 * 60 * 6,
      60 * 60 * 12,
      60 * 60 * 24,
      60 * 60 * 24 * 2,
      60 * 60 * 24 * 7,
      60 * 60 * 24 * 30,
      60 * 60 * 24 * 365,
    ));
    $form['bypass']['timespan'] = array(
      '#type' => 'select',
      '#title' => 'Bypass length',
      '#options' => $bypass_length,
    );
    $form['bypass']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Toggle The "aggregation bypass cookie" For This Browser'),
      '#attributes' => array('onclick' => 'javascript:return advagg_toggle_cookie()'),
      '#submit' => array('advagg_admin_toggle_bypass_cookie'),
    );
    // Add in aggregation bypass cookie javascript.
    $form['#attached']['drupalSettings']['advagg'] = [
        'key' => Crypt::hashBase64(\Drupal::service('private_key')->get()),
    ];
    $form['#attached']['library'][] = 'advagg/admin.operations';

    // Tasks run by cron.
    $form['cron'] = array(
      '#type' => 'fieldset',
      '#title' => t('Cron Maintenance Tasks'),
      '#description' => t('The following 4 operations are ran on cron but you can run them manually here.'),
    );
    $form['cron']['smart_file_flush'] = array(
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#title' => t('Clear All Stale Files'),
      '#description' => t('Remove all stale files. Scan all files in the advagg_css/js directories and remove the ones that have not been accessed in the last 30 days.'),
    );
    $form['cron']['smart_file_flush']['advagg_flush_stale_files'] = array(
      '#type' => 'submit',
      '#value' => t('Remove All Stale Files'),
      '#submit' => array('advagg_admin_flush_stale_files_button'),
    );
    $form['cron']['remove_missing_files'] = array(
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#title' => t('Clear Missing Files From Database'),
      '#description' => t('Scan for missing files and remove the associated entries in the database.'),
    );
    $form['cron']['remove_missing_files']['advagg_remove_missing_files_from_db'] = array(
      '#type' => 'submit',
      '#value' => t('Clear Missing Files From Database'),
      '#submit' => array('advagg_admin_remove_missing_files_from_db_button'),
    );
    $form['cron']['remove_old_aggregates'] = array(
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#title' => t('Delete Unused Aggregates From Database'),
      '#description' => t('Delete aggregates that have not been accessed in the last 6 weeks.'),
    );
    $form['cron']['remove_old_aggregates']['advagg_remove_old_unused_aggregates'] = array(
      '#type' => 'submit',
      '#value' => t('Delete Unused Aggregates From Database'),
      '#submit' => array('advagg_admin_remove_old_unused_aggregates_button'),
    );
    $form['cron']['cleanup_semaphore_table'] = array(
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#title' => t('Delete Orphaned Semaphore Locks'),
      '#description' => t('Delete orphaned/expired advagg locks from the semaphore database table.'),
    );
    $form['cron']['cleanup_semaphore_table']['advagg_cleanup_semaphore_table'] = array(
      '#type' => 'submit',
      '#value' => t('Delete Unused Aggregates From Database'),
      '#submit' => array('advagg_admin_cleanup_semaphore_table_button'),
    );

    // Hide drastic measures as they should not be done unless you really need it.
    $form['drastic_measures'] = array(
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#title' => t('Drastic Measures'),
      '#description' => t('The options below should normally never need to be done.'),
    );
    $form['drastic_measures']['dumb_cache_flush'] = array(
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#title' => t('Clear All Caches'),
      '#description' => t('Remove all entries from the advagg cache bins. Useful if you suspect a cache is not getting cleared.'),
    );
    $form['drastic_measures']['dumb_cache_flush']['advagg_flush_all_caches'] = array(
      '#type' => 'submit',
      '#value' => t('Clear All Caches'),
      '#submit' => array('advagg_admin_clear_all_caches_button'),
    );
    $form['drastic_measures']['dumb_file_flush'] = array(
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#title' => t('Clear All Files'),
      '#description' => t('Remove all generated files. Useful if you think some of the generated files got corrupted and thus need to be deleted.'),
    );
    $form['drastic_measures']['dumb_file_flush']['advagg_flush_all_files'] = array(
      '#type' => 'submit',
      '#value' => t('Remove All Generated Files'),
      '#submit' => array('advagg_admin_clear_all_files_button'),
    );
    $form['drastic_measures']['force_change'] = array(
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#title' => t('Force new aggregates'),
      '#description' => t('Force the creation of all new aggregates by incrementing a global counter. Current value of counter: %value. This is useful if a CDN has cached an aggregate incorrectly as it will force new ones to be used even if nothing else has changed.', array('%value' => advagg_get_global_counter())),
    );
    $form['drastic_measures']['force_change']['increment_global_counter'] = array(
      '#type' => 'submit',
      '#value' => t('Increment Global Counter'),
      '#submit' => array('advagg_admin_increment_global_counter'),
    );
    return parent::buildForm($form, $form_state);
  }

  /**
   * Perform a smart flush.
   */
  public function advaggFlushCache(array &$form, FormStateInterface $form_state) {
    // Run the command.
    module_load_include('inc', 'advagg', 'advagg.cache');
    $flushed = advagg_push_new_changes();
    $config = $this->config('advagg.settings');
    if ($config->get('cache_level') >= 0) {
      // Display a simple message if not in Development mode.
      drupal_set_message(t('Advagg Cache Cleared'));
    }
    else {
      list($css_path) = advagg_get_root_files_dir();
      $parts_uri = $css_path[1] . '/parts';

      // Report back the results.
      foreach ($flushed as $filename => $data) {
        if (strpos($filename, $parts_uri) === 0) {
          // Do not report on css files manged in the parts directory.
          unset($flushed[$filename]);
          continue;
        }
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        drupal_set_message(t('The file %filename has changed. %db_usage aggregates are using this file. %db_count db cache entries and all %type full cache entries have been flushed from the cache bins.', array(
          '%filename' => $filename,
          '%db_usage' => $data[0],
          '%db_count' => $data[1],
          '%type' => $ext,
        )));
      }

      if (empty($flushed)) {
        drupal_set_message(t('No changes found. Nothing was cleared.'));
        return;
      }
    }
  }

  /**
   * Clear out all advagg cache bins.
   */
  function advagg_admin_clear_all_caches_button() {
    // Run the command.
    module_load_include('inc', 'advagg', 'advagg.cache');
    advagg_flush_all_cache_bins();

    // Report back the results.
    drupal_set_message(t('All AdvAgg cache bins have been cleared.'));
    // Clear it again at the end of the request to be sure.
    drupal_register_shutdown_function('advagg_flush_all_cache_bins', FALSE);
    drupal_register_shutdown_function('advagg_push_new_changes');
  }

  /**
   * Clear out all advagg cache bins and clear out all advagg aggregated files.
   */
  function advagg_admin_clear_all_files_button() {
    // Clear out the cache.
    advagg_admin_clear_all_caches_button();

    // Run the command.
    module_load_include('inc', 'advagg', 'advagg.cache');
    list($css_files, $js_files) = advagg_remove_all_aggregated_files();

    // Report back the results.
    drupal_set_message(t('All AdvAgg files have been deleted. %css_count CSS files and %js_count JS files have been removed.', array(
      '%css_count' => count($css_files),
      '%js_count' => count($js_files),
    )));
  }

  /**
   * Clear out all stale advagg aggregated files.
   */
  function advagg_admin_flush_stale_files_button() {
    // Run the command.
    module_load_include('inc', 'advagg', 'advagg.cache');
    list($css_files, $js_files) = advagg_delete_stale_aggregates();

    // Report back the results.
    if (count($css_files) > 0 || count($js_files) > 0) {
      drupal_set_message(t('All stale aggregates have been deleted. %css_count CSS files and %js_count JS files have been removed.', array(
        '%css_count' => count($css_files),
        '%js_count' => count($js_files),
      )));
    }
    else {
      drupal_set_message(t('No stale aggregates found. Nothing was deleted.'));
    }
  }

  /**
   * Clear out all advagg cache bins and increment the counter.
   */
  function advagg_admin_increment_global_counter() {
    // Clear out the cache.
    advagg_admin_clear_all_caches_button();

    // Increment counter.
    module_load_include('inc', 'advagg', 'advagg.cache');
    $new_value = advagg_increment_global_counter();
    drupal_set_message(t('Global counter is now set to %new_value', array('%new_value' => $new_value)));
  }

  /**
   * Scan for missing files and remove the associated entries in the database.
   */
  function advagg_admin_remove_missing_files_from_db_button() {
    module_load_include('inc', 'advagg', 'advagg.cache');

    // Remove aggregates that include missing files.
    $deleted = advagg_remove_missing_files_from_db();
    if (empty($deleted)) {
      drupal_set_message(t('No missing files found or they could not be safely cleared out of the database.'));
    }
    else {
      drupal_set_message(t('Some missing files were found and could be safely cleared out of the database. <pre> @raw </pre>', array('@raw' => print_r($deleted, TRUE))));
    }
  }

  /**
   * Delete aggregates that have not been accessed in the last 6 weeks.
   */
  function advagg_admin_remove_old_unused_aggregates_button() {
    module_load_include('inc', 'advagg', 'advagg.cache');

    // Remove unused aggregates.
    $count = advagg_remove_old_unused_aggregates();
    if (empty($count)) {
      drupal_set_message(t('No old and unused aggregates found. Nothing was deleted.'));
    }
    else {
      drupal_set_message(t('Some old and unused aggregates were found. A total of %count database entries were removed.', array('%count' => $count)));
    }
  }

  /**
   * Delete orphaned/expired advagg locks from the semaphore database table.
   */
  function advagg_admin_cleanup_semaphore_table_button() {
    module_load_include('inc', 'advagg', 'advagg.cache');

    // Delete orphaned/expired advagg locks from the semaphore database table.
    $count = advagg_cleanup_semaphore_table();
    if (empty($count)) {
      drupal_set_message(t('No orphaned advagg semaphore database table locks discovered. Nothing was deleted.'));
    }
    else {
      drupal_set_message(t('Some orphaned advagg semaphore database table locks discovered were found. A total of %count database entries were removed.', array('%count' => $count)));
    }
  }

  /**
   * Set or remove the AdvAggDisabled cookie.
   */
  function advagg_admin_toggle_bypass_cookie($form, &$form_state) {
    $cookie_name = 'AdvAggDisabled';
    $key = Crypt::hashBase64(PrivateKey::get());

    // If the cookie does exist then remove it.
    if (!empty($_COOKIE[$cookie_name]) && $_COOKIE[$cookie_name] == $key) {
      setcookie($cookie_name, '', -1, $GLOBALS['base_path'], '.' . $_SERVER['HTTP_HOST']);
      unset($_COOKIE[$cookie_name]);
      drupal_set_message(t('AdvAgg Bypass Cookie Removed.'));
    }
    // If the cookie does not exist then set it.
    else {
      // Cookie will last for 12 hours.
      setcookie($cookie_name, $key, REQUEST_TIME + $form_state['values']['timespan'], $GLOBALS['base_path'], '.' . $_SERVER['HTTP_HOST']);
      $_COOKIE[$cookie_name] = $key;
      drupal_set_message(t('AdvAgg Bypass Cookie Set for %time.', array('%time' => \Drupal::service("date.formatter")->formatInterval($form_state['values']['timespan']))));
    }
  }

}
