<?php

/**
 * @file
 * Contains \Drupal\advagg\Asset\CssCollectionOptimizer.
 */

namespace Drupal\advagg\Asset;

use Drupal\Core\Asset\AssetCollectionGrouperInterface;
use Drupal\Core\Asset\AssetCollectionOptimizerInterface;
use Drupal\Core\Asset\AssetDumperInterface;
use Drupal\Core\Asset\AssetOptimizerInterface;
use Drupal\Core\Asset\CssCollectionOptimizer as CoreCssCollectionOptimizer;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\State\StateInterface;

/**
 * {@inheritdoc}
 */
class CssCollectionOptimizer extends CoreCssCollectionOptimizer implements AssetCollectionOptimizerInterface {

  /**
   * A config object for the advagg configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   A config factory for retrieving required config objects.
   */
  public function __construct(AssetCollectionGrouperInterface $grouper, AssetOptimizerInterface $optimizer, AssetDumperInterface $dumper, StateInterface $state, ConfigFactoryInterface $config_factory) {
    $this->grouper = $grouper;
    $this->optimizer = $optimizer;
    $this->dumper = $dumper;
    $this->state = $state;
    $this->config = $config_factory->get('advagg.settings');
  }


  /**
   * {@inheritdoc}
   */
  public function optimize(array $css_assets) {

    // Group the assets.
    $css_groups = $this->grouper->group($css_assets);

    // Now optimize (concatenate + minify) and dump each asset group, unless
    // that was already done, in which case it should appear in
    // drupal_css_cache_files.
    // Drupal contrib can override this default CSS aggregator to keep the same
    // grouping, optimizing and dumping, but change the strategy that is used to
    // determine when the aggregate should be rebuilt (e.g. mtime, HTTPS …).
    $map = $this->state->get('drupal_css_cache_files') ?: [];
    $css_assets = [];
    $protocol_relative = $this->config->get('path.convert.absolute_to_protocol_relative');
    $relative = $this->config->get('path.convert.absolute_to_relative');
    $force_https = $this->config->get('path.convert.force_https');
    foreach ($css_groups as $order => $css_group) {
      // We have to return a single asset, not a group of assets. It is now up
      // to one of the pieces of code in the switch statement below to set the
      // 'data' property to the appropriate value.
      $css_assets[$order] = $css_group;
      unset($css_assets[$order]['items']);
      switch ($css_group['type']) {
        case 'file':
          // No preprocessing, single CSS asset: just use the existing URI.
          if (!$css_group['preprocess']) {
            $uri = $css_group['items'][0]['data'];
            if ($relative) {
              $uri = advagg_convert_abs_to_rel($uri);
            }
            $css_assets[$order]['data'] = $uri;
          }
          // Preprocess (aggregate), unless the aggregate file already exists.
          else {
            $key = $this->generateHash($css_group);
            $uri = '';
            if (isset($map[$key])) {
              $uri = $map[$key];
            }
            if (empty($uri) || !file_exists($uri)) {
              // Optimize each asset within the group.
              $data = '';
              $group_file_info = advagg_get_info_on_files(array_column($css_group['items'], 'data'));

              if (array_column($group_file_info, 'dns_prefetch')) {
                $css_assets[$order]['dns_prefetch'] = [];
                foreach ($group_file_info as $file) {
                  if (!empty($file['dns_prefetch'])) {
                    $css_assets[$order]['dns_prefetch'] = array_merge($css_assets[$order]['dns_prefetch'], $file['dns_prefetch']);
                  }
                }
              }
              foreach ($css_group['items'] as $css_asset) {
                $content = $this->optimizer->optimize($css_asset);

                // Allow other modules to modify this file's contents.
                // Call hook_advagg_css_contents_alter().
                \Drupal::moduleHandler()->alter('advagg_css_contents', $content, $css_asset, $group_file_info);
                $data .= $content;
              }
              // Per the W3C specification at
              // http://www.w3.org/TR/REC-CSS2/cascade.html#at-import, @import
              // rules must precede any other style, so we move those to the
              // top.
              $regexp = '/@import[^;]+;/i';
              preg_match_all($regexp, $data, $matches);
              $data = preg_replace($regexp, '', $data);
              $data = implode('', $matches[0]) . $data;
              // Dump the optimized CSS for this group into an aggregate file.
              $uri = $this->dumper->dump($data, 'css');
              // Set the URI for this group's aggregate file.
              $css_assets[$order]['data'] = $uri;
              // Persist the URI for this aggregate file.
              $map[$key] = $uri;
              $this->state->set('drupal_css_cache_files', $map);
            }
            else {
              // Use the persisted URI for the optimized CSS file.
              $css_assets[$order]['data'] = $uri;
            }
            $css_assets[$order]['preprocessed'] = TRUE;
          }
          break;

        case 'external':
          // We don't do any aggregation and hence also no caching for external
          // CSS assets.
          $uri = $css_group['items'][0]['data'];
          if ($force_https) {
            $uri = advagg_path_convert_force_https($uri);
          }
          elseif ($protocol_relative) {
            $uri = advagg_path_convert_protocol_relative($uri);
          }
          $css_assets[$order]['data'] = $uri;
          break;
      }
    }
    return $css_assets;
  }

  /**
   * Deletes all optimized collection assets.
   *
   * Note: Core's deleteAll() only deletes old files not all.
   */
  public function deleteAllReal() {
    $log = [];
    $this->state->delete('system.css_cache_files');
    \Drupal\Core\Cache\Cache::invalidateTags(['library_info', 'advagg_css', 'advagg_js']);
    $delete_all = function($uri) use (&$log) {
      file_unmanaged_delete($uri);
      $log[] = $uri;
    };
    $this->state->delete('system.js_cache_files');
    file_scan_directory('public://css', '/.*/', ['callback' => $delete_all]);
    return $log;
  }

  /**
   * Delete stale optimized collection assets.
   */
  public function deleteStale() {
    $log = [];
    $this->state->delete('system.css_cache_files');
    \Drupal\Core\Cache\Cache::invalidateTags(['library_info', 'advagg_css', 'advagg_js']);
    $delete_stale = function($uri) use (&$log) {
      // Default stale file threshold is 30 days.
      if (REQUEST_TIME - fileatime($uri) > \Drupal::config('system.performance')->get('stale_file_threshold')) {
        file_unmanaged_delete($uri);
        $log[] = $uri;
      }
    };
    file_scan_directory('public://css', '/.*/', array('callback' => $delete_stale));
  }

  /**
   * Delete old optimized collection assets.
   */
  public function deleteOld() {
    $log = [];
    $this->state->delete('system.css_cache_files');
    \Drupal\Core\Cache\Cache::invalidateTags(['library_info', 'advagg_css', 'advagg_js']);
    $delete_old = function($uri) use (&$log) {
      // Default stale file threshold is 30 days.
      // Delete old if > 3 times that.
      if (REQUEST_TIME - filemtime($uri) > \Drupal::config('system.performance')->get('stale_file_threshold') * 3) {
        file_unmanaged_delete($uri);
        $log[] = $uri;
      }
    };
    file_scan_directory('public://css', '/.*/', ['callback' => $delete_old]);
  }

}
