<?php

/**
 * @file
 * Hooks provided by the AdvAgg module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Allow other modules to add in their own settings and hooks.
 *
 * @param array $aggregate_settings
 *   An associative array of hooks and settings used.
 *
 * @see advagg_current_hooks_hash_array()
 * @see advagg_js_minify_advagg_current_hooks_hash_array_alter()
 */
function hook_advagg_current_hooks_hash_array_alter(array &$aggregate_settings) {
  $aggregate_settings['variables']['advagg_js_minify'] = \Drupal::config('advagg_js_minify.settings')->get();
}

/**
 * Allow other modules to modify the contents of individual CSS files.
 *
 * Called once per file at aggregation time.
 *
 * @param string $data
 *   File contents. Depending on settings/modules these may be minified.
 * @param array $css_asset
 *   Asset array.
 * @param array $group_file_info
 *   File information for the whole asset group.
 *
 * @see \Drupal\advagg\Asset\CssCollectionOptimizer::optimize()
 */
function hook_advagg_css_contents_alter(&$data, array &$css_asset, array &$group_file_info) {
  // Remove all font-style rules applying italics.
  preg_replace("/(.*)(font-style\s*:.*italic)(.*)/m", "$0 --> $1 $3", $data);
}

/**
 * Allow other modules to modify the contents of individual JavaScript files.
 *
 * Called once per file at aggregation time.
 *
 * @param string $contents
 *   Raw file data.
 * @param array $js_asset
 *   Asset array.
 *
 * @see \Drupal\advagg\Asset\JsCollectionOptimizer::optimize()
 */
function hook_advagg_js_contents_alter(&$contents, array $js_asset) {
  if ($js_asset['data'] == 'modules/advagg/advagg.admin.js') {
    $contents = str_replace('AdvAgg Bypass Cookie Removed', 'Advanced Aggregates Cookie Removed', $contents);
  }
}

/**
 * Let other modules add/alter additional information about files passed in.
 *
 * @param array $return
 *   An associative array; filename -> data.
 * @param array $cached_data
 *   What data was found in the cache; cache_id -> data.
 * @param bool $bypass_cache
 *   If TRUE the loaded data did not come from the cache.
 *
 * @see advagg_get_info_on_files()
 * @see advagg_advagg_get_info_on_files_alter()
 */
function hook_advagg_get_info_on_files_alter(array &$return, array $cached_data, $bypass_cache) {
  if (!\Drupal::config('advagg.settings')->get('css.ie.limit_selectors')) {
    return;
  }
  $limit_value = \Drupal::config('advagg.settings')->get('css.ie.selector_limit');
  list($css_path, $js_path) = advagg_get_root_files_dir();
  if ($js_path) {
    // This is the js_path array.
  }
  $parts_path = $css_path[1] . '/parts';

  foreach ($return as $filename => &$info) {
    if ($filename) {
      // This is the filename.
    }
    if (empty($info['fileext']) || $info['fileext'] !== 'css') {
      continue;
    }

    // Break large file into multiple small files.
    if ($info['linecount'] > $limit_value) {
      advagg_split_css_file($info);
    }
    elseif (strpos($info['data'], $parts_path) === 0) {
      $info['split'] = TRUE;
    }
  }
  unset($info);
}

/**
 * Tell advagg about other hooks related to advagg.
 *
 * @param array $hooks
 *   Array of hooks related to advagg.
 * @param bool $all
 *   If FALSE get only the subset of hooks that alter the filename/contents.
 *
 * @see advagg_hooks_implemented()
 * @see advagg_bundler_advagg_hooks_implemented_alter()
 */
function hook_advagg_hooks_implemented_alter(array &$hooks, $all) {
  if ($all) {
    $hooks['advagg_bundler_analysis_alter'] = array();
  }
}

/**
 * Let other modules modify the analysis array before it is used.
 *
 * @param array $analysis
 *   An associative array; filename -> data.
 *
 * @see advagg_bundler_analysis()
 */
function hook_advagg_bundler_analysis_alter(array &$analysis) {
  foreach ($analysis as $filename => &$data) {
    if ($filename) {
      // This is the filename.
    }

    // This changes often; 604800 is 1 week.
    if ($data['changes'] > 10 && $data['mtime'] >= REQUEST_TIME - 604800) {
      // Modify the group hash so this doesn't end up in a big aggregate.
      $data['group_hash'];
    }
  }
  unset($data);
}

/**
 * @} End of "addtogroup hooks".
 */
